public class Animal {
    private String name;
    private Integer height;
    private Integer width;
    private Integer weight;

    public Animal(String name, Integer height, Integer width, Integer weight) {
        this.name = name;
        this.height = height;
        this.width = width;
        this.weight = weight;
    }

    public Animal(){
        name = "Жираф";
        height = 100;
        width = 50;
        weight = height * width;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }
}
