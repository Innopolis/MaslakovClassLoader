import org.xml.sax.SAXException;
import xmlLogic.XmlWriterClass;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;

/**
 * Created by sa on 15.02.17.
 */
public class Main {
    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, ParserConfigurationException, IllegalAccessException, InstantiationException, TransformerException, SAXException, IOException, NoSuchFieldException {
        JarLoadUrlFile jarLoadUrlFile =
                new JarLoadUrlFile("https://gitlab.com/Serega852/" +
                        "MalakovClassLoaderAnimal/raw/master/Animal.jar",
                        "local.jar");
        JarClassLoader jarClassLoader = new JarClassLoader("local.jar");
        Object obj = jarClassLoader.loadClass("Animal").newInstance();
        System.out.println(obj);
        XmlWriterClass xmlWriterClass = new XmlWriterClass(obj);
        xmlWriterClass.writeFile("outClass.xml");
        Animal animal = (Animal) xmlWriterClass.readFile("outClass.xml");
        System.out.println(animal.getName());
        //jarLoadUrlFile.loadFile();
        //animalClassLoader.loadClass("Animal");
    }
}
