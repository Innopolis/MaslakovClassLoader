package xmlLogic;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * Created by sa on 10.02.17.
 */
public class XmlWriterClass {
    private Object obj;
    private org.w3c.dom.Document doc;

    public XmlWriterClass(Object obj) throws ParserConfigurationException, NoSuchMethodException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        doc = db.newDocument();
        this.obj = obj;
    }

    private void setField() {
        Element type = doc.createElement("object");
        type.setAttribute("type", obj.getClass().getTypeName());
        for (Field field :
                obj.getClass().getDeclaredFields()) {
            field.setAccessible(true); //Additional line
            Element fieldWrite = doc.createElement("field");
            fieldWrite.setAttribute("type", field.getType().getTypeName());
            fieldWrite.setAttribute("id", field.getName());
            try {
                fieldWrite.setAttribute("value", String.valueOf(field.get(obj)));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            type.appendChild(fieldWrite);
        }
        doc.appendChild(type);
    }

    public void writeFile(String fileName) {
        Transformer trf = null;
        DOMSource src = null;
        FileOutputStream fos = null;
        try {
            trf = TransformerFactory.newInstance().newTransformer();
            src = new DOMSource(doc);
            fos = new FileOutputStream(fileName);
            setField();
            StreamResult result = new StreamResult(fos);
            /*выключить запись в одну строку*/
            trf.setOutputProperty(OutputKeys.INDENT, "yes");
            trf.transform(src, result);
        } catch (TransformerException e) {
            e.printStackTrace(System.out);
        } catch (IOException e) {
            e.printStackTrace(System.out);
        }
    }

    public static Object readFile(String filePath) throws
            ClassNotFoundException, NoSuchFieldException, IllegalAccessException, InstantiationException {
        Document xmlDocument = XmlDocument.createDocumentForParse(filePath);
        Node firstNode = xmlDocument.getFirstChild();
        Class readedClassObject = Class.forName(firstNode.getAttributes().getNamedItem("type").getNodeValue());
        Object result = readedClassObject.newInstance();
        NodeList nodeFields = xmlDocument.getElementsByTagName("field");
        Field[] fields = readedClassObject.getDeclaredFields();
        if (nodeFields.getLength() != fields.length) {
            System.out.println("Не совпадают количество полей сериализуемого " + "класса");
            return null;
        }
        for (int i = 0; i < fields.length; i++) {
            fields[i].setAccessible(true);
        }
        for (int i = 0; i < fields.length; i++) {
            Node nodeFiled = nodeFields.item(i);
            Class fieldClass = Class.forName(nodeFiled.getAttributes().getNamedItem("type").getNodeValue());
            String fieldName = nodeFiled.getAttributes().getNamedItem("id").getNodeValue();
            String fieldValue = nodeFiled.getAttributes().getNamedItem("value").getNodeValue();
            fieldSetValue(result, fields, fieldName, fieldClass, fieldValue);
        }
        return result;
    }

    private static void fieldSetValue(Object result, Field[] fields, String fieldName, Class fieldClass, String fieldValue) {
        for (Field field : fields) {
            if (field.getName().equals(fieldName)) {
                if (field.getType() != String.class) {
                    try {
                        field.set(result, XmlWriterClass.fieldSetValueByExecuteValueOf
                                (fieldClass, fieldValue));
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        field.set(result, fieldValue);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private static Object fieldSetValueByExecuteValueOf(Class fieldClass, String value){
        for (Method method :
                fieldClass.getDeclaredMethods()) {
            if (method.getName().equals("valueOf")){
                if ((method.getParameterTypes().length == 1)
                        && ((method.getParameterTypes()[0] == String.class)))
                    try {
//                        Object obj = fieldClass.newInstance();
                        return method.invoke(fieldClass,value );
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

            }
        }
        return null;
    }
}
