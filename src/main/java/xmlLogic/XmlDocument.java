package xmlLogic;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 * Created by sa on 2/10/2017.
 */
public class XmlDocument {
    private static DocumentBuilderFactory factory = DocumentBuilderFactory
            .newInstance();
    private static  DocumentBuilder builder = null;

    static {
        factory = DocumentBuilderFactory.newInstance();
        try {
            builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    public static Document createDocument(){
        return builder.newDocument();
    }

    public static Document createDocumentForParse(String fileName){
        try {
            return builder.parse(new File(fileName)); // стооитель построил документ
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  null;
    }

    public static void  writeDocument(Document doc, final String path)
            throws TransformerFactoryConfigurationError, TransformerConfigurationException {
        Transformer trf = TransformerFactory.newInstance().newTransformer();
        DOMSource src = new DOMSource(doc);
        try (
                FileOutputStream fos = new FileOutputStream(path);
        ){
            StreamResult result = new StreamResult(fos);
            trf.setOutputProperty(OutputKeys.INDENT, "yes");
            trf.transform(src, result);
        } catch (TransformerException e) {
            e.printStackTrace(System.out);
        } catch (IOException e) {
            e.printStackTrace(System.out);
        }
    }




}



