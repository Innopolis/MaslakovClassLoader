import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by sa on 15.02.17.
 */
public class JarLoadUrlFile {
    private String UrlFile;
    private String fileName;

    public JarLoadUrlFile(String urlFile, String fileName) {
        UrlFile = urlFile;
        this.fileName = fileName;
    }

    public void loadFile() {
        try{
        URL connection = new URL(UrlFile);
        HttpURLConnection urlconn;
        urlconn = (HttpURLConnection) connection.openConnection();
        urlconn.setRequestMethod("GET");
        urlconn.connect();
        InputStream in = null;
        in = urlconn.getInputStream();
        OutputStream writer = new FileOutputStream(fileName);
        byte buffer[] = new byte[512];

        while (in.read(buffer)>0)
        {
            writer.write(buffer);
        }
        writer.flush();
        writer.close();
        in.close();
    } catch (IOException e) {
        System.out.println(e);
    }
    }
}
